package com.devcamp.s50.task56c40.customerinvoiceapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task56c40.customerinvoiceapi.models.Customer;
import com.devcamp.s50.task56c40.customerinvoiceapi.services.CustomerService;

@RestController
@CrossOrigin
public class CustomerController {
     private CustomerService customerService;

     public CustomerController(){
          this.customerService = new CustomerService();
     }

     @GetMapping("/customers")
     public ArrayList<Customer> getAllCustomers(){
          return customerService.getAllCustomer();
     }

}
