package com.devcamp.s50.task56c40.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s50.task56c40.customerinvoiceapi.models.Invoice;

@Service
public class InvoiceService {
     private ArrayList<Invoice> invoices;

     public InvoiceService(){
          invoices = new ArrayList<>();
          CustomerService customerService = new CustomerService();
          invoices.add(new Invoice(1, customerService.getAllCustomer().get(0), 10.5));
          invoices.add(new Invoice(2, customerService.getAllCustomer().get(1), 20.5));
     }

     public ArrayList<Invoice> getAllInvoices(){
          return invoices;
     }
}
