package com.devcamp.s50.task56c40.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s50.task56c40.customerinvoiceapi.models.Customer;

@Service
public class CustomerService {
     private ArrayList<Customer> customers;
     
     public CustomerService(){
          customers = new ArrayList<>();
          customers.add(new Customer(1, "Tan Loc", 10000));
          customers.add(new Customer(2, "My Cam", 20000));
     }

     public ArrayList<Customer> getAllCustomer(){
          return customers;
     }
}
