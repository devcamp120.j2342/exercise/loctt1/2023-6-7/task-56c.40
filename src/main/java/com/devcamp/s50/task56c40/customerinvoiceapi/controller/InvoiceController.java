package com.devcamp.s50.task56c40.customerinvoiceapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task56c40.customerinvoiceapi.models.Invoice;
import com.devcamp.s50.task56c40.customerinvoiceapi.services.InvoiceService;

@RestController
@CrossOrigin
public class InvoiceController {
     private InvoiceService invoiceService;

     public InvoiceController(){
          this.invoiceService = new InvoiceService();
     }

     @GetMapping("/invoices")
     public ArrayList<Invoice> getAllInvoices0(){
          return invoiceService.getAllInvoices();
     }
}
