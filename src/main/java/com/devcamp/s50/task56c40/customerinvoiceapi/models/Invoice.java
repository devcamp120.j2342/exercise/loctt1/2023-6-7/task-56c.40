package com.devcamp.s50.task56c40.customerinvoiceapi.models;

public class Invoice {
     private int id;
     private Customer customer;
     private double amount;
     
     public Invoice() {
     }

     public Invoice(int id, Customer customer, double amount) {
          this.id = id;
          this.customer = customer;
          this.amount = amount;
     }

     public int getId() {
          return id;
     }

     public Customer getCustomer() {
          return customer;
     }

     public double getAmount() {
          return amount;
     }

     public void setCustomer(Customer customer) {
          this.customer = customer;
     }

     public void setAmount(double amount) {
          this.amount = amount;
     }

     public int getCustomerId(){
          return this.customer.getId();
     }

     public String getCustomerName(){
          return this.customer.getName();
     } 

     public int getCustomerDiscount(){
          return this.customer.getDiscount();
     }

     public double getAmountAfterDiscount(){
          double discountPercentage = customer.getDiscount();
          double discountAmount = (discountPercentage / 100) * amount;
          double amountAfterDiscount = amount - discountAmount;
          return amountAfterDiscount;
     }
     
}
